'use strict';

require('dotenv').config();
const { userStore, crypto } = require('./lib');

const app = require('./app');

app.listen(process.env.HTTP_PORT, process.env.HTTP_HOST, () => {
  // Before, we must to store a user and thus be able to perform the next requests
  userStore.insert('testeapple@ioasys.com.br', crypto('12341234', 'testeapple@ioasys.com.br'));
  // eslint-disable-next-line no-console
  console.info(`\tServer running at ${process.env.HTTP_HOST}:${process.env.HTTP_PORT}`);
});
