'use strict';

const db = require('../../db/connection');
const { formatter } = require('../../lib');
const enterpriseQueries = require('../../db/queries/enterprise');
const enterpriseTypeQueries = require('../../db/queries/enterprise_type');

exports.getEnterprise = (req, res) => {
  db.query(enterpriseQueries.selectOne, req.params.id, (err, data) => {
    if (err) return res.status(500).send(err);
    // Adapts the output to the expected format as following: enterprise_type: {id: 1, enterprise_type_name: 'something'}
    data[0]['enterprise_type'] = { id: data[0]['id_type'], enterprise_type_name: data[0]['enterprise_type_name'] };
    delete data[0]['enterprise_type_name'];
    delete data[0]['id_type'];
    res.status(200).send(data);
  });
};

exports.getFilteredEnterprise = (req, res) => {
  const { enterprise_types: enterpriseTypes, name } = req.query;

  db.query(enterpriseQueries.selectMany, [`%${name}%`, enterpriseTypes], (err, data) => {
    if (err) return res.status(500).send(err);
    res.status(200).send(data);
  });
};

exports.insertEnterprise = (req, res) => {
  const enterprise = req.body;
  const values = {};
  // Transforms all keys to snake case format.
  Object.keys(enterprise).forEach(key => {
    values[formatter.camelToSnake(key)] = enterprise[key];
  });

  db.query(enterpriseQueries.insert, values, (err, data) => {
    if (err) return res.status(500).send(err);
    res.status(200).send(data);
  });
};

exports.insertType = (req, res) => {
  db.query(enterpriseTypeQueries.insert, [0, req.body.enterpriseType], (err, data) => {
    if (err) return res.status(500).send(err);
    res.status(200).send(data);
  });
};
