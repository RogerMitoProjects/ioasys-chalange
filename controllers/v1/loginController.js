'use strict';

const jwt = require('jsonwebtoken');
const { userStore, crypto } = require('../../lib');

exports.login = (req, res) => {
  const { username, password } = req.body;

  const user = userStore.select(username);
  if (!user) return res.status(401).send('Unauthorized');

  if (crypto(password, username) === user.password) {
    // Create a new toke with the company CNPJ document in the payload.
    // SHA 512 is used as a set of cryptographic hash functions.
    jwt.sign(
      { username },
      process.env.TOKEN_SECRETE,
      { algorithm: 'HS512', expiresIn: process.env.TOKEN_EXPIRATION },
      (err, token) => {
        if (err) return res.status(500).send(err);
        return res.status(200).send(token);
      }
    );
  } else {
    res.status(401).send('Unauthorized');
  }
};
