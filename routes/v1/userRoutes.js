'use strict';

const router = require('express').Router();
const { loginController } = require('../../controllers/v1');

router.post('/auth/sign_in', loginController.login);

module.exports = router;
