'use strict';

const router = require('express').Router();
const { checkAuth } = require('../../middleware');

// Only login routes are spared from authentication checks. For this work, a user must be stored in the API before.
router.use('/users/', require('./userRoutes'));

// Auth middleware
router.use(checkAuth);
router.use('/enterprises/', require('./enterpriseRoutes'));

module.exports = router;
