'use strict';

const router = require('express').Router();
const { enterpriseController } = require('../../controllers/v1');
const { enterpriseValidator, enterpriseTypeValidator } = require('../../validators');

router.post('/type', enterpriseTypeValidator.insertType, enterpriseController.insertType);
router.get('/:id', enterpriseValidator.getEnterprise, enterpriseController.getEnterprise);
router.post('/', enterpriseValidator.insertEnterprise, enterpriseController.insertEnterprise);
router.get('/', enterpriseValidator.getFilteredEnterprise, enterpriseController.getFilteredEnterprise);

module.exports = router;
