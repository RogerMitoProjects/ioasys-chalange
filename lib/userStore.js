'use strict';

const userStore = new Map();

exports.insert = (username, password) => userStore.set(username, { username, password });

exports.select = username => userStore.get(username);
