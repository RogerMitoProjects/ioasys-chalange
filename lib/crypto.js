'use strict';

const crypto = require('crypto');

/**
 * @description Makes a hash by sha512.
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 * @returns Password hashing.
 */
module.exports = (password, salt) => {
  const hash = crypto.createHmac('sha512', salt).update(password); // Hashing algorithm sha512
  const value = hash.digest('hex');
  return value;
};
