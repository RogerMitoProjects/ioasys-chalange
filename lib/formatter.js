'use strict';

exports.camelToSnake = str => str.replace(/[A-Z]/g, (_match, p1) => '_'.concat(str.charAt(p1)));

exports.snakeToCamel = str => str.replace(/_.{1}/g, (_match, p1) => str.charAt(p1 + 1).toUpperCase());
