# Desafio ioasys

### Restrições de projeto e observações

- É necessária a criação de um usuário para que a etapa de login seja executada com sucesso. Tal processo é realizado automaticamente durante a inicialização do servidor.
- Para que todas as consultas sejam executadas com sucesso é necessário inserir o token de acesso obtido na etapa de login no cabeçalho das requisições posteriores.

- Rotas de cadastro para `enterprise` e `enterprise_type` foram criadas apenas a fim de proporcionar o preenchimento inicial da base de dados e dessa forma permitir a execução das operações requiridas pelo desafio.

- O script de criação da base de dados está disponível em: /db/build/schema.sql.

- O funcionamento da API requer uma instância local do MySQL.

Alguns exemplos de consumo da API:

**Consulta de empresa**

Requisição (GET): `http://localhost:3004/api/v1/enterprises/1`

**Login**

Requisição (POST): `http://localhost:3004/api/v1/auth/sign_in`

Corpo:

```json
{
  "username": "testeapple@ioasys.com.br",
  "password": "12341234"
}
```

### Container mySQL

Comando de inicialização:
`$ docker run --name ioasys-mysql -e MYSQL_USER=root -e MYSQL_ROOT_PASSWORD=123 -p 3306:3306 -d mysql:5.7`

### Variáveis de ambiente

O arquivo `.env` deve estar presente na raiz do projeto. Ele é necessário para a execução dos testes de unidade em ambiente local; seu conteúdo pode ser conferido logo abaixo:

```text
#environment
ENVIRONMENT='local'

# database
MYSQL_HOST='127.0.0.1'
MYSQL_PORT=3306
MYSQL_USER='root'
MYSQL_ROOT_PASSWORD='123'
MYSQL_DATABASE='ioasys'

# server
HTTP_HOST='127.0.0.1'
HTTP_PORT=3004

# authentication
TOKEN_EXPIRATION='36h'
TOKEN_SECRETE='@34%q4T'
TOKEN=''
```

### Construção

`npm run build`

### Inicialização

`npm start`

### Teste

`npm run test`

# Instruções fornecidas pelo avaliador

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto Empresas.

### O QUE FAZER ?

- Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por email o resultado do seu teste.

### ESCOPO DO PROJETO

- Deve ser criada uma API em **NodeJS** ou **Ruby on Rails**.
- A API deve fazer o seguinte:

1. Login e acesso de Usuário já registrado;
2. Para o login usamos padrões **JWT** ou **OAuth 2.0**;
3. Listagem de Empresas
4. Detalhamento de Empresas
5. Filtro de Empresas por nome e tipo

### Informações Importantes

- A API deve funcionar exatamente da mesma forma que a disponibilizada na collection do postman, mais abaixo os acessos a API estarão disponíveis em nossoservidor.
- Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);
- Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;
- Mantenha a mesma estrutura do postman em sua API, ou seja, ela deve ter os mesmo atributos, respostas, rotas e tratamentos, funcionando igual ao nossoexemplo.
- Quando seu código for finalizado e disponibilizado para validarmos, vamos subir em nosso servidor e realizar a integração com o app.
- Independente de onde conseguiu chegar no teste é importante disponibilizar seu fonte para analisarmos.
- É obrigatório utilização de Banco de Dados MySql/PostgreSQL

### Dados para Teste

- Servidor: http://empresas.ioasys.com.br/
- Versão da API: v1
- Usuário de Teste: testeapple@ioasys.com.br
- Senha de Teste : 12341234

### Dicas

- Documentação JWT https://jwt.io/
- Frameworks NodeJS:
  - https://expressjs.com/pt-br/
  - https://sailsjs.com/
- Guideline rails http://guides.rubyonrails.org/index.html
- Componente de autenticação https://github.com/lynndylanhurley/devise_token_auth
