'use strict';

const jwt = require('jsonwebtoken');
const { userStore } = require('../lib');

module.exports = (req, res, next) => {
  try {
    let token = req.headers.authorization;
    if (!token) return res.status(401).send('Unauthorized');
    token = token.split(' ')[1];
    jwt.verify(token, process.env.TOKEN_SECRETE, (err, decoded) => {
      if (err) return res.status(500).send('Bad token content');

      const user = userStore.select(decoded.username);
      if (user) next();
      else res.status(401).send('Unauthorized');
    });
  } catch (err) {
    res.status(500).send(err);
  }
};
