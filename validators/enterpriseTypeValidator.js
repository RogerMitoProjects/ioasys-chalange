'use strict';

const { check, validationResult } = require('express-validator/check');

exports.insertType = [
  check('enterpriseType').exists(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });
    next();
  }
];
