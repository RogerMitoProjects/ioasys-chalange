'use strict';

const { validationResult, check, query, param } = require('express-validator/check');

exports.insertEnterprise = [
  check('enterpriseName', 'city', 'country').exists(),
  check('enterpriseType').isNumeric(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });
    next();
  }
];

exports.getFilteredEnterprise = [
  query('enterprise_types').isInt({ min: 1 }, query('name').exists()),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });
    next();
  }
];

exports.getEnterprise = [
  param('id').exists(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });
    next();
  }
];
