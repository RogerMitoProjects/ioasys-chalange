'use strict';

require('dotenv').config();
const request = require('request');
const { should } = require('chai');

const url = `http://${process.env.HTTP_HOST}:${process.env.HTTP_PORT}/api/v1`;

const user = {
  username: 'testeapple@ioasys.com.br',
  password: '12341234'
};

const options = {
  json: true,
  body: {},
  headers: {}
};

describe('Module: init', () => {
  before(done => {
    require('../index');
    done();
  });

  it('Method: Login', done => {
    options.body = user;
    request.post(`${url}/users/auth/sign_in`, options, (err, res) => {
      should().not.exist(err);
      should().exist(res.body);
      process.env.TOKEN = res.body;
      if (process.env.ENVIRONMENT === 'local') {
        require('./api');
      }
      done();
    });
  });
});
