'use strict';

const request = require('request');
const { should, expect } = require('chai');

const url = `http://${process.env.HTTP_HOST}:${process.env.HTTP_PORT}/api/v1`;

const enterprise = {
  id: 0,
  enterpriseName: 'Alpaca Samka SpA',
  description:
    'Alpaca Samka uses alpaca fibres for our “Slow Fashion Project” in association with the Aymaras of the Chilean Andes, producing sustainable luxury accessories and garments using traditional Andean methods and British weaving patterns. We are part of the Inward Investment Program and have been recognised by international organisations. ',
  emailEnterprise: '',
  facebook: '',
  twitter: '',
  linkedin: '',
  phone: '',
  ownEnterprise: false,
  photo: '/uploads/enterprise/photo/2/WhatsApp_Image_2017-10-31_at_13.47.22.jpeg',
  value: 0,
  shares: 100,
  sharePrice: 10000,
  ownShares: 0,
  city: 'Viña del Mar',
  country: 'Chile',
  enterpriseType: null
};
const enterpriseType = {
  id: 0,
  enterpriseType: 'health and food'
};
const options = {
  json: true,
  body: {},
  headers: { Authorization: `Bearer ${process.env.TOKEN}` }
};

describe('Module: ioasys', () => {
  it('Method: Insert enterprise type', done => {
    options.body = enterpriseType;
    request.post(`${url}/enterprises/type`, options, (err, res) => {
      should().not.exist(err);
      expect(res.body.insertId).to.be.a('number');
      enterpriseType.id = res.body.insertId;
      done();
    });
  });

  it('Method: Insert enterprise', done => {
    enterprise.enterpriseType = enterpriseType.id;
    options.body = enterprise;
    request.post(`${url}/enterprises`, options, (err, res) => {
      should().not.exist(err);
      should().exist(res.body);
      expect(res.body.insertId).to.be.a('number');
      enterprise.id = res.body.insertId;
      done();
    });
  });

  it(`Method: Get enterprise ${enterprise.id}`, done => {
    options.body = {};
    request.get(`${url}/enterprises/${enterprise.id}`, options, (err, res) => {
      should().not.exist(err);
      should().exist(res.body);
      expect(res.body.length).to.be.equal(1);
      expect(res.body[0].id).to.be.equal(enterprise.id);
      done();
    });
  });

  it('Method: Get filtered enterprise', done => {
    request.get(`${url}/enterprises?enterprise_types=${enterpriseType.id}&name=Alpaca`, options, (err, res) => {
      should().not.exist(err);
      should().exist(res.body);
      expect(res.body.length).to.be.above(0);
      expect(res.body[0].id).to.be.equal(enterprise.id);
      done();
    });
  });
});
