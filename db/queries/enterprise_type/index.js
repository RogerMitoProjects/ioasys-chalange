'use strict';

module.exports = {
  selectOne: require('./selectOne'),
  insert: require('./insert')
};
