'use strict';

module.exports =
  'SELECT a.*, b.id as id_type, b.enterprise_type_name FROM enterprise AS a INNER JOIN enterprise_type AS b ON a.enterprise_type = b.id WHERE a.enterprise_name like ? and b.id = ?';
