'use strict';

module.exports = {
  insert: require('./insert'),
  selectOne: require('./selectOne'),
  selectMany: require('./selectMany')
};
