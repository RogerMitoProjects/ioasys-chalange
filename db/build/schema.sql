-- -----------------------------------------------------
-- Schema ioasys
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ioasys` DEFAULT CHARACTER SET utf8 ;
USE `ioasys` ;

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';


-- -----------------------------------------------------
-- Table `ioasys`.`enterprise_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ioasys`.`enterprise_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `enterprise_type_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ioasys`.`enterprise`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ioasys`.`enterprise` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `enterprise_name` VARCHAR(100) NOT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `email_enterprise` VARCHAR(50) NULL DEFAULT NULL,
  `facebook` VARCHAR(50) NULL DEFAULT NULL,
  `twitter` VARCHAR(50) NULL DEFAULT NULL,
  `linkedin` VARCHAR(50) NULL DEFAULT NULL,
  `phone` VARCHAR(14) NULL DEFAULT NULL,
  `own_enterprise` TINYINT(4) NOT NULL DEFAULT '0',
  `photo` VARCHAR(100) NULL DEFAULT NULL,
  `value` DECIMAL(12,2) NOT NULL DEFAULT '0.00',
  `shares` INT(11) NOT NULL DEFAULT '0',
  `share_price` DECIMAL(12,2) NOT NULL DEFAULT '0.00',
  `own_shares` INT(11) NOT NULL DEFAULT '0',
  `city` VARCHAR(100) NOT NULL,
  `country` VARCHAR(100) NOT NULL,
  `enterprise_type` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_enterprise_enterprise_type_idx` (`enterprise_type` ASC),
  CONSTRAINT `fk_enterprise_enterprise_type`
    FOREIGN KEY (`enterprise_type`)
    REFERENCES `ioasys`.`enterprise_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
